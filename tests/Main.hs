{-# LANGUAGE ImportQualifiedPost #-}

module Main where

import Pbt.Properties qualified as Pbt

import Control.Monad (replicateM)
import Data.Proxy (Proxy (..))
import Pbt.Driver (DriverState (..))
import Pbt.Expr (genApp, genExpr, genLit, genVar)
import Pbt.Expr.Utility (betaReduce, utility1)
import Pbt.Optimize (test)
import Pbt.Properties (functions1, gen, genPalka, genTuplesBoxed, outputsMatch)
import Test.Tasty (
  defaultIngredients,
  defaultMainWithIngredients,
  testGroup,
 )
import Test.Tasty.QuickCheck (testProperty)

main :: IO ()
main = do
  ctx <- test
  defaultMainWithIngredients defaultIngredients $
    testGroup
      "PBTs"
      [ -- testProperty "default" $ outputsMatch genDefaultInt
        testProperty "tuple" $ outputsMatch (gen functions1 ctx)
      ]

-- testProperty "palka" Pbt.palka
-- testProperty "outputsMatch" Pbt.defaultOutputsMatch
