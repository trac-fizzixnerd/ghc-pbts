{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ExplicitNamespaces #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
-- OverloadedStrings required for doctests
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}

module Pbt.Expr.Utility where

import Data.Bifunctor (Bifunctor (..))
import Data.Foldable (Foldable (..))
import Data.List.NonEmpty (NonEmpty (..))
import Data.Maybe (fromMaybe, maybeToList)
import Pbt.Expr (
  Binder (..),
  CaseAlternative (..),
  Context (..),
  Expr (..),
  Id (..),
  SomeExpr (..),
  SomeScopedId,
  SomeTypeProxy,
  caseBinder,
  caseExpr,
  interpretToHsSrc,
 )
import Test.QuickCheck.Gen (Gen)
import Type.Reflection (
  Typeable,
  eqTypeRep,
  typeRep,
  type (:~~:) (..),
 )

-- * Utility Functions over @Expr@

-- | e.g. @(\\x -> (\\x -> x)) (\\x -> x)@ is has 3 new local vars, all named @x@.

--- >>> countNewLocalVars $ (Wild @(Int -> Int) "x" --> Wild @Int "x" --> Var @Int "x") <!> (Wild "x" --> Var "x")
-- 3
countNewLocalVars :: Expr a -> Int
countNewLocalVars (Lam binder body) =
  countNewLocalVars body + countNewLocalVarsBinder binder
countNewLocalVars (App f x) =
  countNewLocalVars f + countNewLocalVars x
countNewLocalVars (Tup x y) =
  countNewLocalVars x + countNewLocalVars y
countNewLocalVars (UTup x y) =
  countNewLocalVars x + countNewLocalVars y
countNewLocalVars (Case e alts) =
  countNewLocalVars e
    + sum ((\alt -> countNewLocalVarsBinder (caseBinder alt) + (\(SomeExpr e) -> countNewLocalVars e) (caseExpr alt)) <$> alts)

countNewLocalVarsBinder :: Binder a -> Int
countNewLocalVarsBinder (Wild _) = 1
countNewLocalVarsBinder (Tuple _) = 2
countNewLocalVarsBinder (UTuple _) = 2

-- | @True@ iff @ident@ occurs _unshadowed by deeper binders_ within the expression.

--- >>> occurs "x" $ Wild @Int "x" :--> Wild @Int "x" :--> Var @Int "x"
-- False
--- >>> occurs "y" $ Var @Int "y"
-- True
occurs :: Id a -> Expr b -> Bool
occurs ident (Lam binder body) = not (occursBinder ident binder) && occurs ident body
occurs (Id _ name) (Var (Id _ name')) = name == name'
occurs _ (Lit _ _) = False
occurs ident (App f x) = occurs ident f || occurs ident x
occurs ident (Tup x y) = occurs ident x || occurs ident y
occurs ident (UTup x y) = occurs ident x || occurs ident y
occurs ident (Case e as) =
  occurs ident e
    || foldl'
      (\acc alt -> acc || (not (occursBinder ident (caseBinder alt)) && (\(SomeExpr e) -> occurs ident e) (caseExpr alt)))
      False
      as

occursBinder :: Id a -> Binder b -> Bool
occursBinder (Id _ name) (Wild (Id _ name')) = name == name'
occursBinder (Id _ name) (Tuple (Id _ nameLeft, Id _ nameRight)) = name == nameLeft || name == nameRight
occursBinder (Id _ name) (UTuple (Id _ nameLeft, Id _ nameRight)) = name == nameLeft || name == nameRight

{- | If is a lambda that (upon beta-reduction) discards its argument then @True@, else @False@.
This function isn't magical; it doesn't know that the global symbol @const@ discards its
argument, for example.
-}
isConstantFunction :: Expr a -> Bool
isConstantFunction (Lam binder body) =
  case binder of
    Wild ident -> not $ occurs ident (betaReduce body)
    Tuple (identLeft, identRight) -> not $ occurs identLeft (betaReduce body) || occurs identRight (betaReduce body)
    UTuple (identLeft, identRight) -> not $ occurs identLeft (betaReduce body) || occurs identRight (betaReduce body)
isConstantFunction e = False

countConstantFunctions :: Expr a -> Int
countConstantFunctions l@(Lam binder body) = countConstantFunctions body + if isConstantFunction l then 1 else 0
countConstantFunctions (App f x) = countConstantFunctions f + countConstantFunctions x
countConstantFunctions (Tup x y) = countConstantFunctions x + countConstantFunctions y
countConstantFunctions (UTup x y) = countConstantFunctions x + countConstantFunctions y
countConstantFunctions (Case e as) = countConstantFunctions e + sum ((\(SomeExpr e) -> countConstantFunctions e) . caseExpr <$> as)
countConstantFunctions _ = 0

{- | This doesn't properly count every identity function, only ones that look
 like @\x -> x@ in source code.
-}
isIdentityFunction :: Expr a -> Bool
isIdentityFunction (Lam (Wild (Id _ name)) (Var (Id _ name'))) = name == name'
isIdentityFunction _ = False

countIdentityFunctions :: Expr a -> Int
countIdentityFunctions (App f x) = countIdentityFunctions f + countIdentityFunctions x
countIdentityFunctions (Tup x y) = countIdentityFunctions x + countIdentityFunctions y
countIdentityFunctions (UTup x y) = countIdentityFunctions x + countIdentityFunctions y
countIdentityFunctions (Case e as) = countIdentityFunctions e + sum ((\(SomeExpr e) -> countIdentityFunctions e) . caseExpr <$> as)
countIdentityFunctions e = if isIdentityFunction e then 1 else 0

countConstants :: Expr a -> Int
countConstants (Var _) = 0
countConstants (Lit _ _) = 1
countConstants (App f x) = countConstants f + countConstants x
countConstants (Lam _ body) = countConstants body
countConstants (Tup x y) = countConstants x + countConstants y
countConstants (UTup x y) = countConstants x + countConstants y
countConstants (Case e as) = countConstants e + sum ((\(SomeExpr e) -> countConstants e) . caseExpr <$> as)

maxFunctionDepth :: Expr a -> Int
maxFunctionDepth (App f x) = 1 + max (maxFunctionDepth f) (maxFunctionDepth x)
maxFunctionDepth (Lam _ body) = maxFunctionDepth body
maxFunctionDepth (Tup x y) = max (maxFunctionDepth x) (maxFunctionDepth y)
maxFunctionDepth (UTup x y) = max (maxFunctionDepth x) (maxFunctionDepth y)
maxFunctionDepth (Case e as) = maxFunctionDepth e + maximum ((\(SomeExpr e) -> maxFunctionDepth e) . caseExpr <$> as)
maxFunctionDepth _ = 0

-- FIXME: Implement for Tup/Case
countUsedLocalVars :: Expr a -> Int
countUsedLocalVars (Lam (Wild ident) body) =
  if occurs ident body
    then 1
    else 0
countUsedLocalVars (App f x) =
  countUsedLocalVars f + countUsedLocalVars x
countUsedLocalVars _ = 0

-- FIXME: Implement
countUsedGlobalVars :: Expr a -> Int
countUsedGlobalVars e = undefined

-- | @True@ iff the result of @betaReduce e@ is a @Lit@.
reducesToLiteral :: Expr a -> Bool
reducesToLiteral e =
  case betaReduce e of
    Lit _ _ -> True
    _ -> False

-- | Replace @ident@ with @replacee@ in @e@.  Guaranteed type-correct.
replace :: (Typeable a, Typeable b) => Id a -> Expr a -> Expr b -> Expr b
replace (Id _ ident :: Id a) (replacee :: Expr a) e@(Var (Id _ var) :: Expr b)
  | ident == var
  , Just HRefl <- eqTypeRep (typeRep @a) (typeRep @b) =
      replacee
  | otherwise = e
replace _ _ l@(Lit _ _) = l
replace ident replacee (App f x) = App (replace ident replacee f) (replace ident replacee x)
replace (ident :: Id a) replacee (Lam (binder :: Binder b) body)
  | Just HRefl <- eqTypeRep (typeRep @a) (typeRep @b)
  , occursBinder ident binder =
      Lam binder body
  | otherwise = Lam binder (replace ident replacee body)
replace ident replacee (Tup x y) =
  Tup (replace ident replacee x) (replace ident replacee y)
replace ident replacee (UTup x y) =
  UTup (replace ident replacee x) (replace ident replacee y)
replace (ident :: Id a) replacee (Case e as) =
  Case (replace ident replacee e) $
    ( \case
        (alt :: CaseAlternative c b)
          | not $ occursBinder ident (caseBinder alt) ->
              case alt of
                WildAlt b be -> WildAlt b (replace ident replacee be)
                TupleAlt b be -> TupleAlt b (replace ident replacee be)
                UTupleAlt b be -> UTupleAlt b (replace ident replacee be)
          | otherwise ->
              alt
    )
      <$> as

betaReduce :: Expr a -> Expr a
betaReduce (App f arg) =
  case betaReduce f of
    Lam binder body ->
      case binder of
        Wild ident -> betaReduce (replace ident arg body)
        Tuple (identLeft, identRight)
          | Tup x y <- betaReduce arg -> replace identLeft x (replace identRight y body)
          | otherwise -> betaReduce body
        UTuple (identLeft, identRight)
          | UTup x y <- betaReduce arg -> replace identLeft x (replace identRight y body)
          | otherwise -> betaReduce body
    f' -> App f' (betaReduce arg)
betaReduce (Lam binder body) = Lam binder (betaReduce body)
betaReduce (Tup x y) = Tup (betaReduce x) (betaReduce y)
betaReduce (UTup x y) = UTup (betaReduce x) (betaReduce y)
betaReduce (Case e ((WildAlt b f) :| [])) = betaReduce (App f e)
betaReduce (Case e as) = Case (betaReduce e) (betaReduceAlternative <$> as)
betaReduce e = e

betaReduceAlternative :: CaseAlternative b a -> CaseAlternative b a
betaReduceAlternative (WildAlt b x) = WildAlt b (betaReduce x)
betaReduceAlternative (TupleAlt b x) = TupleAlt b (betaReduce x)
betaReduceAlternative (UTupleAlt b x) = UTupleAlt b (betaReduce x)

-- | Warning: uses equality of generated source text currently!
isBetaReduced :: (Show a) => Expr a -> Bool
isBetaReduced e = interpretToHsSrc e == interpretToHsSrc (betaReduce e)

tryBetaReduce :: (Show a) => Expr a -> Maybe (Expr a)
tryBetaReduce e = if isBetaReduced e then Nothing else Just (betaReduce e)

shrinkExprVia :: (Show a) => (forall ty. (Show ty) => Expr ty -> Maybe (Expr ty)) -> Expr a -> [Expr a]
shrinkExprVia s e = maybeToList (s e)

utility1 :: Expr a -> Double
utility1 e =
  if reducesToLiteral e
    then 0
    else
      fromIntegral (countUsedLocalVars e * countConstants e * maxFunctionDepth (betaReduce e))
        / fromIntegral (1 + countIdentityFunctions e * countConstantFunctions e * maxFunctionDepth e)

mkContext1 :: (Int, [SomeScopedId], [(Int, SomeTypeProxy)], Int) -> (Int, a, a, a, a, a, a) -> Context a
mkContext1 (size, scopedIds, genTypeProxy, sizeDivider) (scopedIdFreq, lamFreq, localVarFreq, appFreq, varFreq, litFreq, caseFreq) =
  Context size inScopeIds genTypeProxy lamFreq localVarFreq appFreq varFreq litFreq caseFreq sizeDivider
 where
  inScopeIds = fmap (scopedIdFreq,) scopedIds

mkContext2 :: (Int, [(Int, SomeScopedId)], [(Int, SomeTypeProxy)], Int) -> (a, a, a, a, a, a) -> Context a
mkContext2 (size, inScopeIds, genTypeProxy, sizeDivider) (lamFreq, localVarFreq, appFreq, varFreq, litFreq, caseFreq) =
  Context size inScopeIds genTypeProxy lamFreq localVarFreq appFreq varFreq litFreq caseFreq sizeDivider
