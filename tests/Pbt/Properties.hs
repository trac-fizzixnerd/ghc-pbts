{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}

module Pbt.Properties (
  averageBy,
  outputsMatch,
  genPalka,
  genTuplesBoxed,
  genTypeProxyUnboxedTuples,
  functionsUnboxedTuples,
  simpleContext1,
  simpleContext2,
  gen,
  guardBy,
  judge,
  functions1,
  genTypeProxy1,
) where

import Control.Monad (replicateM, when)
import Data.Proxy (Proxy (..))
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Text.IO qualified as Text
import Debug.Trace (traceM)
import Pbt.Driver (
  Driver (..),
  DriverState (..),
  genDriver,
  makeModuleText,
  moduleDance,
  shrinkDriverVia,
  writeModuleFile,
 )
import Pbt.Expr (
  Context (..),
  Expr,
  Id (..),
  ScopedId (..),
  SomeScopedId (..),
  SomeTypeProxy (..),
  Unboxed,
  genExpr,
  interpretToHsSrc,
 )
import Pbt.Expr.Utility (betaReduce, mkContext1, mkContext2, tryBetaReduce, utility1)
import Test.QuickCheck.Gen (Gen, frequency, sample')
import Test.QuickCheck.Property (Property, forAll, forAllShrinkBlind, forAllShrinkShow, ioProperty)
import Type.Reflection (Typeable)

genTypeProxy1 :: (Num a) => [(a, SomeTypeProxy)]
genTypeProxy1 =
  [ (1000, SomeTypeProxy (Proxy @()))
  , (16000, SomeTypeProxy (Proxy @Int))
  , (8000, SomeTypeProxy (Proxy @Double))
  , (16000, SomeTypeProxy (Proxy @(Int -> Int)))
  , (8000, SomeTypeProxy (Proxy @((Int -> Int) -> Int -> Int)))
  ]

genTypeProxy2 :: (Num a) => [(a, SomeTypeProxy)]
genTypeProxy2 =
  [ (8, SomeTypeProxy (Proxy @Int))
  , (1, SomeTypeProxy (Proxy @[Int]))
  , (8, SomeTypeProxy (Proxy @(Int -> [Int] -> [Int])))
  , (8, SomeTypeProxy (Proxy @([Int] -> [Int])))
  ]

genTypeProxyPalka :: (Num a) => [(a, SomeTypeProxy)]
genTypeProxyPalka =
  [ (4, SomeTypeProxy (Proxy @Int))
  , (4, SomeTypeProxy (Proxy @[Int]))
  , (4, SomeTypeProxy (Proxy @((Int -> [Int]) -> Int -> [Int])))
  , (4, SomeTypeProxy (Proxy @(Int -> [Int])))
  , (4, SomeTypeProxy (Proxy @(Int -> [Int] -> [Int])))
  , (4, SomeTypeProxy (Proxy @([Int] -> [Int])))
  ]

genTypeProxyUnboxedTuples :: (Num a) => [(a, SomeTypeProxy)]
genTypeProxyUnboxedTuples =
  [ (8000, SomeTypeProxy (Proxy @Int))
  , (1000, SomeTypeProxy (Proxy @(Int, Int)))
  , (4000, SomeTypeProxy (Proxy @(Int -> Int -> (Int, Int))))
  , (32000, SomeTypeProxy (Proxy @(Int -> Int -> Int)))
  , (32000, SomeTypeProxy (Proxy @(Int -> Int)))
  , (4000, SomeTypeProxy (Proxy @(Int -> (Int, Int))))
  , (4000, SomeTypeProxy (Proxy @(Int -> Int -> Unboxed (Int, Int))))
  , (4000, SomeTypeProxy (Proxy @(Int -> Unboxed (Int, Int))))
  , (1000, SomeTypeProxy (Proxy @((Int, Int) -> Unboxed (Int, Int))))
  ]

-- * Ints

iadd :: ScopedId (Int -> Int -> Int)
iadd = GlobalId (Id True "+") "Prelude" "|+|"

imul :: ScopedId (Int -> Int -> Int)
imul = GlobalId (Id True "*") "Prelude" "|*|"

idiv :: ScopedId (Int -> Int -> Int)
idiv = GlobalId (Id False "div") "Prelude" "idiv"

isucc :: ScopedId (Int -> Int)
isucc = GlobalId (Id False "succ") "Prelude" "isucc"

idInt :: ScopedId (Int -> Int)
idInt = GlobalId (Id False "id") "Prelude" "idInt"

seqIntInt :: ScopedId (Int -> Int -> Int)
seqIntInt = GlobalId (Id False "seq") "Prelude" "seqIntInt"

undefinedInt :: ScopedId Int
undefinedInt = GlobalId (Id False "undefined") "Prelude" "undefinedInt"

-- * Floats

fadd :: ScopedId (Double -> Double -> Double)
fadd = GlobalId (Id True "+") "Prelude" ".+"

fmul :: ScopedId (Double -> Double -> Double)
fmul = GlobalId (Id True "*") "Prelude" ".*"

fdiv :: ScopedId (Double -> Double -> Double)
fdiv = GlobalId (Id True "/") "Prelude" "./"

-- * Float + Int

fromIntegral' :: ScopedId (Int -> Double)
fromIntegral' = GlobalId (Id False "fromIntegral") "Prelude" "itod"

round' :: ScopedId (Double -> Int)
round' = GlobalId (Id False "round") "Prelude" "dtoi"

-- * [Int]s

undefinedIntList :: ScopedId [Int]
undefinedIntList = GlobalId (Id False "undefined") "Prelude" "undefinedIntList"

cons :: ScopedId (Int -> [Int] -> [Int])
cons = WiredInSyntax (Id True ":") "|:|"

nil :: ScopedId [Int]
nil = GlobalId (Id False "mempty") "Prelude" "nil"

fmapIntToInt :: ScopedId ((Int -> Int) -> [Int] -> [Int])
fmapIntToInt = GlobalId (Id False "fmap") "Prelude" "fmapIntToInt"

fmapIntToDouble :: ScopedId ((Int -> Double) -> [Int] -> [Double])
fmapIntToDouble = GlobalId (Id False "fmap") "Prelude" "fmapIntToDouble"

fmapDoubleToDouble :: ScopedId ((Double -> Double) -> [Double] -> [Double])
fmapDoubleToDouble = GlobalId (Id False "fmap") "Prelude" "fmapDoubleToDouble"

foldlIntToListInt :: ScopedId (([Int] -> Int -> [Int]) -> [Int] -> [Int] -> [Int])
foldlIntToListInt = GlobalId (Id False "foldl") "Prelude" "foldlIntoToListInt"

foldlIntToInt :: ScopedId ((Int -> Int -> Int) -> Int -> [Int] -> [Int])
foldlIntToInt = GlobalId (Id False "foldl") "Prelude" "foldlIntToInt"

lengthInt :: ScopedId ([Int] -> Int)
lengthInt = GlobalId (Id False "length") "Prelude" "lengthInt"

seqIntIntList :: ScopedId (Int -> [Int] -> [Int])
seqIntIntList = GlobalId (Id False "seq") "Prelude" "seqIntIntList"

seqIntListIntList :: ScopedId ([Int] -> [Int] -> [Int])
seqIntListIntList = GlobalId (Id False "seq") "Prelude" "seqIntListIntList"

idIntList :: ScopedId ([Int] -> [Int])
idIntList = GlobalId (Id False "id") "Prelude" "idIntList"

idIntIntList :: ScopedId ((Int -> [Int]) -> Int -> [Int])
idIntIntList = GlobalId (Id False "id") "Prelude" "idIntIntList"

-- * Tuples

mkUTuple :: ScopedId (Int -> Int -> Unboxed (Int, Int))
mkUTuple = LocalDefinition (Id False "mkUTuple") "\\x y -> (# x, y #)"

mkTuple :: ScopedId (Int -> Int -> (Int, Int))
mkTuple = LocalDefinition (Id False "mkTuple") "\\x y -> (x, y)"

mkRange :: ScopedId ((Int, Int) -> Unboxed (Int, Int))
mkRange = LocalDefinition (Id False "mkRange") "\\(x, y) -> (# x - y, x + y #)"

mkRange' :: ScopedId (Int -> Unboxed (Int, Int))
mkRange' = LocalDefinition (Id False "mkRange'") "\\x -> (# -x, x #)"

guardBy :: (Monad m) => (a -> Bool) -> m a -> m a
guardBy p g = do
  x <- g
  if p x then pure x else guardBy p g

averageBy :: (a -> Double) -> [a] -> Double
averageBy f xs = sum (f <$> xs) / fromIntegral (length xs + 1)

functionsPalka :: [SomeScopedId]
functionsPalka =
  [ SomeScopedId seqIntInt
  , SomeScopedId seqIntListIntList
  , SomeScopedId seqIntIntList
  , SomeScopedId idInt
  , SomeScopedId idIntList
  , SomeScopedId idIntIntList
  , SomeScopedId undefinedInt
  , SomeScopedId undefinedIntList
  ]

functionsUnboxedTuples :: [(Int, SomeScopedId)]
functionsUnboxedTuples =
  [ (1000, SomeScopedId mkUTuple)
  , (16000, SomeScopedId mkTuple)
  , (32000, SomeScopedId iadd)
  , (32000, SomeScopedId seqIntInt)
  , (1000, SomeScopedId mkRange)
  , (8000, SomeScopedId mkRange')
  , (1000, SomeScopedId undefinedInt)
  ]

functions1 :: [(Int, SomeScopedId)]
functions1 =
  fmap
    (1000,)
    [ SomeScopedId iadd
    , SomeScopedId isucc
    , SomeScopedId imul
    , SomeScopedId idiv
    , SomeScopedId fadd
    , SomeScopedId fmul
    , SomeScopedId fdiv
    , SomeScopedId fromIntegral'
    , SomeScopedId round'
    , SomeScopedId cons
    , SomeScopedId nil
    , SomeScopedId fmapIntToInt
    , SomeScopedId fmapIntToDouble
    , SomeScopedId fmapDoubleToDouble
    , SomeScopedId foldlIntToListInt
    , SomeScopedId lengthInt
    , SomeScopedId mkTuple
    ]

functions2 :: [SomeScopedId]
functions2 =
  [ SomeScopedId cons
  ]

outputsMatch :: (Typeable ty, Show ty) => Gen (DriverState ty) -> Property
outputsMatch s =
  forAllShrinkShow
    s
    (shrinkDriverVia tryBetaReduce)
    (Text.unpack . Text.concat . fmap interpretToHsSrc . expressions)
    $ \s' -> ioProperty $ do
      (g, o) <- moduleDance s'
      putStrLn "Non-Beta Reduced Expressions:"
      print (expressions s')
      putStr "AverageUtility: "
      print $ judge s'
      putStrLn "Beta Reduced Expressions:"
      print (betaReduce <$> expressions s')
      pure (g == o)

genPalka :: Gen (DriverState ([Int] -> [Int]))
genPalka =
  let mkVariableContext = mkContext1 (100, functionsPalka, genTypeProxyPalka, 2)
      initVariables = (1000, 250, 1000, 2000, 25, 25, 2500)
   in genDriver (<> " $ ((1 :: Int) : 2 : undefined))") functionsPalka (genExpr @([Int] -> [Int]) (mkVariableContext initVariables))

simpleContext1 :: [SomeScopedId] -> [(Int, SomeTypeProxy)] -> Context Int
simpleContext1 functions genType = mkContext1 (100, functions, genType, 2) (1000, 250, 1000, 2000, 25, 25, 100)

simpleContext2 :: [(Int, SomeScopedId)] -> [(Int, SomeTypeProxy)] -> Context Int
simpleContext2 functions genType = mkContext2 (500, functions, genType, 2) (862, 1035, 2805, 75, 519, 109)

{- genDefaultInt :: Gen (DriverState Int)
genDefaultInt = do
  let context = simpleContext1 functions1 genTypeProxy1
  genDriver id functions1 (genExpr @Int context)
 -}
genTuplesBoxed :: Context Int -> Gen (DriverState (Int, Int))
genTuplesBoxed ctx = genDriver (\zn -> "(case " <> zn <> " of ( x, y ) -> x)") (fmap snd functionsUnboxedTuples) (genExpr @(Int, Int) ctx)

gen :: [(a, SomeScopedId)] -> Context Int -> Gen (DriverState (Int, Int))
gen funcs ctx = genDriver (\zn -> "(case " <> zn <> " of ( x, y ) -> x)") (fmap snd funcs) (genExpr @(Int, Int) ctx)

judge :: DriverState ty -> Double
judge = averageBy utility1 . expressions