{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE ExplicitNamespaces #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# OPTIONS_GHC -fno-cse -fno-full-laziness #-}

module Pbt.Driver (
  Driver (..),
  Import (..),
  DriverState (..),
  pushExpression,
  makeModuleText,
  writeModuleFile,
  shrinkDriverVia,
  moduleDance,
  genDriver,
) where

import Control.Monad.State.Strict (MonadState, StateT (StateT), modify')
import Data.Functor.Identity (Identity (..))
import Data.Kind (Type)
import Data.List qualified as List
import Data.Proxy (Proxy)
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Text.IO qualified as Text
import Debug.Trace (trace)
import Pbt.Expr qualified as Pbt
import Pbt.Expr.Utility (shrinkExprVia)
import System.Exit (ExitCode (..))
import System.FilePath ((</>))
import System.IO (IOMode (WriteMode), hClose, withBinaryFile)
import System.IO.Temp qualified as Temp
import System.IO.Unsafe (unsafeDupablePerformIO)
import System.Process qualified as Process
import Test.QuickCheck.Gen (Gen, listOf, listOf1, vectorOf)
import Type.Reflection (TypeRep (..), Typeable, splitApps, typeRep, type (:~~:) (HRefl))
import Type.Reflection qualified as Reflection

data Import = Import
  { moduleName :: !Text
  , qualifiedName :: Maybe Text
  , symbols :: [Text]
  }
  deriving (Show)

data DriverState ty = DriverState
  { functions :: ![Pbt.SomeScopedId]
  , expressions :: ![Pbt.Expr ty]
  , showExpression :: !(Text -> Text)
  , goldenModuleName :: !Text
  , optimizedModuleName :: !Text
  , languagePragmas :: ![Text]
  , moduleImports :: ![Import]
  , goldenOptionsGhc :: ![Text]
  , optimizedOptionsGhc :: ![Text]
  , goldenFilename :: !Text
  , optimizedFilename :: !Text
  }

newtype Driver ty a = Driver {unDriver :: StateT (DriverState ty) Identity a}
  deriving newtype (Functor, Applicative, Monad, MonadState (DriverState ty))

pushExpression :: Pbt.Expr ty -> Driver ty ()
pushExpression e = modify' (\s -> s{expressions = e : expressions s})

makeImportText :: Import -> Text
makeImportText i = "import " <> qualify (qualifiedName i) (moduleName i)
 where
  qualify = maybe id (\qual mid -> "qualified " <> mid <> " as " <> qual)

makeExpressionText :: (Show ty, Typeable ty) => (Int, Pbt.Expr ty) -> Text
makeExpressionText (n, e :: Pbt.Expr ty) =
  Text.unlines
    [ zn <> " :: " <> showType (typeRep :: TypeRep ty)
    , zn <> " = " <> Pbt.interpretToHsSrc e
    ]
 where
  zn = "z" <> Text.pack (show n)

makeFunctionText :: (Typeable ty) => Pbt.ScopedId ty -> Text
makeFunctionText (Pbt.LocalDefinition (Pbt.Id isInfix fText :: Pbt.Id fType) definition) =
  Text.unlines
    [ brackets isInfix fText <> " :: " <> showType (typeRep :: TypeRep fType)
    , brackets isInfix fText <> " = " <> definition
    ]
makeFunctionText (Pbt.GlobalId (Pbt.Id isInfix fText :: Pbt.Id fType) qualifier moduleLocalName) =
  Text.unlines
    [ brackets isInfix moduleLocalName <> " :: " <> showType (typeRep :: TypeRep fType)
    , brackets isInfix moduleLocalName <> " = " <> brackets isInfix (qualifier <> "." <> fText)
    ]
makeFunctionText (Pbt.LocalId (Pbt.Id _ f)) = error $ "Impossible: Cannot give a global name to a local ID: " <> Text.unpack f
makeFunctionText (Pbt.WiredInSyntax (Pbt.Id isInfix fText :: Pbt.Id fType) moduleLocalName) =
  Text.unlines
    [ brackets isInfix moduleLocalName <> " :: " <> showType (typeRep :: TypeRep fType)
    , brackets isInfix moduleLocalName <> " = " <> brackets isInfix fText
    ]

showType :: TypeRep a -> Text
showType tr =
  case splitApps tr of
    (unboxedTyCon, [Reflection.SomeTypeRep (xs :: TypeRep (xs :: kxs))])
      | (tupleTyCon, [Reflection.SomeTypeRep (x :: TypeRep x), Reflection.SomeTypeRep (y :: TypeRep y)]) <- splitApps xs
      , Just HRefl <- Reflection.typeRepKind x `Reflection.eqTypeRep` typeRep @Type
      , Just HRefl <- Reflection.typeRepKind y `Reflection.eqTypeRep` typeRep @Type
      , Reflection.TypeRep <- x
      , Reflection.TypeRep <- y
      , Just HRefl <- typeRep @(Pbt.Unboxed (x, y)) `Reflection.eqTypeRep` tr ->
          "(# " <> showType (typeRep @x) <> ", " <> showType (typeRep @y) <> " #)"
      | otherwise -> Text.pack $ show tr
    (tyCon, xs) ->
      let
        shownXs = (\(Reflection.SomeTypeRep tr) -> "( " <> showType tr <> " ) ") <$> xs
        showTyCon =
          Text.pack
            -- FIXME: There has GOT to be a better way...
            ( case Reflection.tyConName tyCon of
                "FUN" -> "->"
                "Tuple2" -> "(,)"
                _ -> Reflection.tyConName tyCon
            )
       in
        "( " <> showTyCon <> " ) " <> Text.concat shownXs

brackets :: Bool -> Text -> Text
brackets isInfix x = if isInfix then "(" <> x <> ")" else x

makeFunctionText' :: Pbt.SomeScopedId -> Text
makeFunctionText' (Pbt.SomeScopedId f) = makeFunctionText f

makePrintStatement :: (Typeable ty) => (Text -> Text) -> (Int, Pbt.Expr ty) -> Text
makePrintStatement shower (n, _ :: Pbt.Expr ty) =
  "  print $ " <> shower zn <> ";"
 where
  zn = "z" <> Text.pack (show n)

makeModuleText :: (Show ty, Typeable ty) => Bool -> DriverState ty -> Text
makeModuleText isGolden s =
  let
    optionsGhc = "{-# OPTIONS_GHC " <> Text.unwords (if isGolden then goldenOptionsGhc s else optimizedOptionsGhc s) <> " #-}"
    pragmaLines = (\p -> "{-# LANGUAGE " <> p <> " #-}") <$> languagePragmas s
    moduleLine = "module " <> (if isGolden then goldenModuleName s else optimizedModuleName s) <> " where"
    imports = makeImportText <$> moduleImports s
    iexprs = zip [0 ..] (expressions s)
   in
    Text.unlines $
      [optionsGhc]
        <> pragmaLines
        <> [moduleLine]
        <> imports
        <> (makeFunctionText' <$> functions s)
        <> (makeExpressionText <$> iexprs)
        <> ["main = do {"]
        <> (makePrintStatement (showExpression s) <$> iexprs)
        <> ["}"]

writeModuleFile :: (Show ty, Typeable ty) => Bool -> DriverState ty -> IO FilePath
writeModuleFile isGolden s = do
  let moduleText = makeModuleText isGolden s
  let fp = Text.unpack $ if isGolden then goldenFilename s else optimizedFilename s
  withBinaryFile fp WriteMode (`Text.hPutStrLn` moduleText)
  pure fp

compileFileSimple :: FilePath -> FilePath -> IO ()
compileFileSimple fp out = do
  (_, _, _, processHandle) <- Process.createProcess (Process.proc "ghc" ["--make", fp, "-o", out])
  ec <- Process.waitForProcess processHandle
  case ec of
    ExitFailure failureCode -> error $ "compileFileSimple failed with exit code: " <> show failureCode
    ExitSuccess -> pure ()

runFileSimple :: FilePath -> IO (Text, Text)
runFileSimple fp = do
  (_, Just hOut, Just hErr, processHandle) <-
    Process.createProcess
      (Process.proc fp [])
        { Process.std_out =
            Process.CreatePipe
        , Process.std_err =
            Process.CreatePipe
        }
  _ <- Process.waitForProcess processHandle
  out <- Text.hGetContents hOut
  err <- Text.hGetContents hErr
  out `seq` err `seq` do
    hClose hOut
    hClose hErr
    pure (out, err)

moduleDance :: (Show ty, Typeable ty) => DriverState ty -> IO ((Text, Text), (Text, Text))
moduleDance s = do
  Temp.withSystemTempDirectory "ghc-pbts" $ \tempDirectory -> do
    moduleDance'
      tempDirectory
      ( s
          { goldenFilename = Text.pack $ tempDirectory </> Text.unpack (goldenFilename s)
          , optimizedFilename = Text.pack $ tempDirectory </> Text.unpack (optimizedFilename s)
          }
      )
 where
  moduleDance' tempDirectory ds = do
    golden <- writeModuleFile True ds
    let goldenOut = tempDirectory </> "M"
    compileFileSimple golden goldenOut
    g@(goldenOut, goldenErr) <- runFileSimple goldenOut
    optimized <- writeModuleFile False ds
    let optimizedOut = tempDirectory </> "M"
    compileFileSimple optimized optimizedOut
    o@(optimizedOut, optimizedErr) <- runFileSimple optimizedOut
    g `seq` o `seq` pure (g, o)

genDriver :: (Text -> Text) -> [Pbt.SomeScopedId] -> Gen (Pbt.Expr ty) -> Gen (DriverState ty)
genDriver showExpr functions gExpr = do
  examples <- vectorOf 10 gExpr
  pure $
    DriverState
      { functions = functions
      , expressions = examples
      , showExpression = showExpr
      , goldenModuleName = "Main"
      , optimizedModuleName = "Main"
      , languagePragmas = ["LambdaCase", "UnboxedTuples"]
      , moduleImports = []
      , goldenOptionsGhc = []
      , optimizedOptionsGhc = ["-O2"]
      , goldenFilename = "M.hs"
      , optimizedFilename = "M.hs"
      }

shrinkDriverVia :: (Show ty) => (forall a. (Show a) => Pbt.Expr a -> Maybe (Pbt.Expr a)) -> DriverState ty -> [DriverState ty]
shrinkDriverVia exprShrinker s =
  let exprs = expressions s
      exprToDriver e = s{expressions = [e]}
      shrunkExprs = shrinkExprVia exprShrinker =<< exprs
   in exprToDriver <$> shrunkExprs