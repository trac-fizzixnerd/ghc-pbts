{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE ExplicitNamespaces #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Pbt.Expr (
  Expr (..),
  Binder (..),
  CaseAlternative (..),
  Id (..),
  ScopedId (..),
  SomeExpr (..),
  SomeScopedId (..),
  SomeTypeProxy (..),
  GenExprWithContext (..),
  Unboxed (..),
  Context (..),
  caseBinder,
  caseExpr,
  genExpr,
  genLit,
  genVar,
  genApp,
  interpretToHsSrc,
  interpretToHsSrc',
  pattern (:-->),
  pattern (:<!>),
) where

import Control.Concurrent (threadDelay)
import Control.Monad (replicateM)
import Data.Function ((&))
import Data.Functor.Identity (Identity (..))
import Data.Kind (Type)
import Data.List.NonEmpty (NonEmpty (..))
import Data.Proxy (Proxy (..))
import Data.String (IsString (fromString))
import Data.Text (Text, pack)
import Data.Text qualified as Text
import Data.Text.IO qualified as Text
import Data.Traversable (fmapDefault, foldMapDefault)
import Debug.Trace (traceShowM)
import GHC.Generics (Generic)
import Test.QuickCheck (Arbitrary (arbitrary))
import Test.QuickCheck.Gen (Gen, chooseInt, frequency, vectorOf)
import Type.Reflection (TypeRep, Typeable, eqTypeRep, someTypeRep, typeRep, pattern TypeRep, type (:~~:) (..))
import Type.Reflection qualified as Reflection

data Id a where
  Id :: Bool -> Text -> Id a
  deriving (Show)

instance IsString (Id a) where
  fromString = Id False . fromString

instance Functor Id where
  fmap = fmapDefault

instance Foldable Id where
  foldMap = foldMapDefault

instance Traversable Id where
  traverse _ (Id isInfix name) = pure $ Id isInfix name

-- | Marker for 'Unboxed' types, so that we don't have to use levity polymorphism everwhere.
newtype Unboxed a = Unboxed a
  deriving stock (Typeable)
  -- Show is newtype derived to avoid extraneous constructors
  deriving newtype (Show, Eq, Ord)

-- | A pattern used to name the parameter(s) of a 'Case' or 'CaseUTup'
data Binder a where
  Wild :: Id a -> Binder a
  Tuple :: (Typeable a, Typeable b) => (Id a, Id b) -> Binder (a, b)
  UTuple :: (Typeable a, Typeable b) => (Id a, Id b) -> Binder (Unboxed (a, b))

-- | A binder that cannot be a UTuple.  Used to bind variables in 'Lam's.
type LamBinder a = Binder a

data Expr a where
  Var ::
    Id a ->
    Expr a
  Lit ::
    (a -> Text) ->
    a ->
    Expr a
  App ::
    (Typeable a, Typeable b) =>
    Expr (a -> b) ->
    Expr a ->
    Expr b
  Lam ::
    (Typeable a, Typeable b) =>
    LamBinder a ->
    Expr b ->
    Expr (a -> b)
  Tup ::
    (Typeable a, Typeable b) =>
    Expr a ->
    Expr b ->
    Expr (a, b)
  UTup ::
    (Typeable a, Typeable b) =>
    Expr a ->
    Expr b ->
    Expr (Unboxed (a, b))
  Case ::
    (Typeable a, Typeable b) =>
    Expr a ->
    NonEmpty (CaseAlternative a b) ->
    Expr b

data CaseAlternative a b where
  WildAlt :: Binder a -> Expr (a -> b) -> CaseAlternative a b
  TupleAlt :: (Typeable c, Typeable d) => Binder (c, d) -> Expr (c -> d -> b) -> CaseAlternative (c, d) b
  UTupleAlt :: (Typeable c, Typeable d) => Binder (Unboxed (c, d)) -> Expr (c -> d -> b) -> CaseAlternative (Unboxed (c, d)) b

caseBinder :: CaseAlternative a b -> Binder a
caseBinder (WildAlt b _) = b
caseBinder (TupleAlt b _) = b
caseBinder (UTupleAlt b _) = b

caseExpr :: CaseAlternative a b -> SomeExpr
caseExpr (WildAlt _ e) = SomeExpr e
caseExpr (TupleAlt _ e) = SomeExpr e
caseExpr (UTupleAlt _ e) = SomeExpr e

infixr 4 :-->
pattern b :--> e = Lam b e

infixl 1 :<!>
pattern f :<!> e = App f e

data SomeExpr = forall a. SomeExpr (Expr a)

data ScopedId (a :: Type) where
  GlobalId ::
    { unscopedId :: Id a
    , qualifier :: Text
    , moduleLocalName :: Text
    } ->
    ScopedId a
  LocalId :: Id a -> ScopedId a
  WiredInSyntax :: Id a {- Original symbol -} -> Text {- Module local name -} -> ScopedId a
  LocalDefinition :: Id a -> Text -> ScopedId a
  deriving (Show)

localName :: ScopedId a -> Text
localName (GlobalId{moduleLocalName = moduleLocalName}) = moduleLocalName
localName (LocalId (Id _ localIdName)) = localIdName
localName (WiredInSyntax _ moduleLocalName) = moduleLocalName
localName (LocalDefinition (Id _ localDefName) _) = localDefName

data SomeScopedId = forall a. (Typeable a) => SomeScopedId (ScopedId a)

instance Show SomeScopedId where
  show (SomeScopedId si) = show si

data SomeTypeProxy = forall a. (Typeable a, GenExprWithContext a) => SomeTypeProxy (Proxy a)

instance Show SomeTypeProxy where
  show (SomeTypeProxy (_ :: Proxy a)) = show $ typeRep @a

interpretToHsSrc :: Expr a -> Text
interpretToHsSrc (Var (Id isInfix ident)) = "(" <> ident <> ")"
interpretToHsSrc (Lit shower x) = "(" <> shower x <> ")"
interpretToHsSrc (App f x) = "(" <> interpretToHsSrc f <> " " <> interpretToHsSrc x <> ")"
interpretToHsSrc (Lam b body) = "(\\" <> interpretBinder b <> " -> " <> interpretToHsSrc body <> ")"
interpretToHsSrc (Tup x y) = "(" <> interpretToHsSrc x <> ", " <> interpretToHsSrc y <> ")"
interpretToHsSrc (UTup x y) = "(# " <> interpretToHsSrc x <> ", " <> interpretToHsSrc y <> " #)"
interpretToHsSrc (Case scrutinee (b :| branches)) = "(case " <> interpretToHsSrc scrutinee <> " of { " <> interpretBranch b <> Text.concat (interpretBranch <$> branches) <> "})"

interpretBranch :: CaseAlternative a b -> Text
interpretBranch (WildAlt binder f) =
  interpretBinder binder
    <> " -> "
    <> interpretToHsSrc f
    <> " "
    <> interpretBinder binder
    <> " ; "
interpretBranch (TupleAlt binder f) =
  interpretBinder binder
    <> " -> "
    <> interpretToHsSrc f
    <> " "
    <> ( case binder of
          Tuple (x, y) -> interpretId x <> " " <> interpretId y
       )
    <> " ; "
interpretBranch (UTupleAlt binder f) =
  interpretBinder binder
    <> " -> "
    <> interpretToHsSrc f
    <> " "
    <> ( case binder of
          UTuple (x, y) -> interpretId x <> " " <> interpretId y
       )
    <> " ; "

interpretBinder :: Binder a -> Text
interpretBinder (Wild x) = interpretId x
interpretBinder (Tuple (x, y)) = "(" <> interpretId x <> ", " <> interpretId y <> ")"
interpretBinder (UTuple (x, y)) = "(# " <> interpretId x <> ", " <> interpretId y <> " #)"

interpretId :: Id a -> Text
interpretId (Id _ ident) = ident

interpretToHsSrc' :: SomeExpr -> Text
interpretToHsSrc' (SomeExpr e) = interpretToHsSrc e

instance (Show ty) => Show (Expr ty) where
  show x = Text.unpack $ interpretToHsSrc x

data Context a = Context
  { size :: !Int
  , inScopeIds :: [(Int, SomeScopedId)]
  , genTypes :: [(Int, SomeTypeProxy)]
  , newLambdaFreq :: !a
  , newLocalVarFreq :: !a
  , newAppFreq :: !a
  , newVarFreq :: !a
  , newLitFreq :: !a
  , newCaseFreq :: !a
  , sizeDivider :: !Int
  }
  deriving stock (Generic, Functor, Show)

genTypeProxy :: Context Int -> Gen SomeTypeProxy
genTypeProxy ctx = frequency $ fmap pure <$> genTypes ctx

filterIds :: (Typeable a) => TypeRep a -> [(Int, SomeScopedId)] -> [(Int, ScopedId a)]
filterIds x someIds = filterIds' x someIds []
 where
  filterIds' :: (Typeable a) => TypeRep a -> [(Int, SomeScopedId)] -> [(Int, ScopedId a)] -> [(Int, ScopedId a)]
  filterIds' x [] !acc = acc
  filterIds' x ((w, SomeScopedId (y :: ScopedId b)) : ys) !acc =
    case eqTypeRep x (typeRep @b) of
      Just HRefl ->
        filterIds' x ys ((w, y) : acc)
      Nothing ->
        filterIds' x ys acc

genExpr :: (Typeable a, GenExprWithContext a) => Context Int -> Gen (Expr a)
genExpr ctx =
  frequency
    [ (newAppFreq ctx * size ctx, genApp ctx')
    , (newVarFreq ctx * size ctx, genVar Proxy ctx')
    , (newLitFreq ctx * size ctx + 1, genLit ctx')
    , (newCaseFreq ctx * size ctx, genCase ctx')
    ]
 where
  ctx' = ctx{size = size ctx `div` sizeDivider ctx}

genCase :: (Typeable a, GenExprWithContext a) => Context Int -> Gen (Expr a)
genCase ctx = do
  SomeTypeProxy (_ :: Proxy b) <- genTypeProxy ctx
  scrutinee :: Expr b <- genExpr ctx
  (binder :: Binder b, ctx') <- genBinder typeRep ctx
  case binder of
    Wild ident -> do
      primaryBranch :: Expr (b -> a) <- genExpr ctx'
      pure (Case scrutinee (WildAlt binder primaryBranch :| []))
    Tuple (x :: Id x, y :: Id y) -> do
      primaryBranch :: Expr (x -> y -> a) <- genExpr ctx'
      pure (Case scrutinee (TupleAlt binder primaryBranch :| []))
    UTuple (x :: Id x, y :: Id y) -> do
      primaryBranch :: Expr (x -> y -> a) <- genExpr ctx'
      pure (Case scrutinee (UTupleAlt binder primaryBranch :| []))

genBinder :: TypeRep a -> Context Int -> Gen (Binder a, Context Int)
genBinder (TypeRep :: TypeRep a) ctx = do
  case Reflection.splitApps (typeRep @a) of
    (tupleTyCon, [Reflection.SomeTypeRep (x :: TypeRep (x :: kx)), Reflection.SomeTypeRep (y :: TypeRep (y :: ky))])
      | Just HRefl <- Reflection.typeRepKind x `eqTypeRep` typeRep @Type
      , Just HRefl <- Reflection.typeRepKind y `eqTypeRep` typeRep @Type
      , TypeRep <- x
      , TypeRep <- y
      , Just HRefl <- typeRep @(x, y) `eqTypeRep` typeRep @a -> do
          idX <- genFreshishId x ctx
          idY <- genFreshishId y ctx
          let ctx' =
                ctx
                  { inScopeIds =
                      (newLocalVarFreq ctx * fromIntegral (length (inScopeIds ctx) + 1) * size ctx + 1, SomeScopedId (LocalId idX))
                        : (newLocalVarFreq ctx * fromIntegral (length (inScopeIds ctx)) * size ctx + 1, SomeScopedId (LocalId idY))
                        : inScopeIds ctx
                  }
          pure (Tuple (idX, idY), ctx')
    _ -> do
      freshishId <- genFreshishId typeRep ctx
      let ctx' = ctx{inScopeIds = (newLocalVarFreq ctx * fromIntegral (length (inScopeIds ctx)) * size ctx + 1, SomeScopedId (LocalId freshishId)) : inScopeIds ctx}
      pure (Wild freshishId, ctx')

genFreshishId :: TypeRep a -> Context Int -> Gen (Id a)
genFreshishId pxy ctx = do
  -- FIXME: Sometimes breaks if you pick the same int more than once and the types don't match!
  i <- chooseInt (0, 1000000)
  pure . Id False $ "x" <> tshow i

genVar :: (Typeable a, GenExprWithContext a) => Proxy a -> Context Int -> Gen (Expr a)
genVar (pxy :: Proxy a) ctx =
  frequency $
    (1, genLit ctx) : (fmap (pure . Var . Id False . localName) <$> filterIds (typeRep @a) (inScopeIds ctx))

genLit :: (Typeable a, GenExprWithContext a) => Context Int -> Gen (Expr a)
genLit = genExprWithContext

genApp :: (Typeable a, GenExprWithContext a) => Context Int -> Gen (Expr a)
genApp ctx = do
  SomeTypeProxy (_ :: Proxy b) <- genTypeProxy ctx
  newLambda :: Expr (b -> a) <- genExprWithContext ctx
  case newLambda of
    Lam binder body -> do
      -- This erases the isInfix info
      let possibleIds = fmap (pure . Var . Id False . localName) <$> filterIds (typeRep :: TypeRep (b -> a)) (inScopeIds ctx)
      fun <- frequency $ (newLambdaFreq ctx * size ctx + 1, pure $ Lam binder body) : possibleIds
      appBody <- genExpr ctx
      pure $ App fun appBody
    _ -> error "impossible: `genExprWithContext @tag @(b -> a)` generated a non-lambda."

class GenExprWithContext a where
  genExprWithContext :: Context Int -> Gen (Expr a)

instance GenExprWithContext () where
  genExprWithContext _ = pure $ Lit tshow ()

tshow :: (Show a) => a -> Text
tshow = Text.pack . show

instance GenExprWithContext Double where
  genExprWithContext _ =
    fmap (Lit tshow) . (*)
      <$> frequency
        ( fmap pure
            <$> [ (1, 0.0)
                , (2, 1e-27)
                , (2, 0.000001)
                , (2, 0.1)
                , (4, 1.0)
                , (4, 100.0)
                , (2, 1e54)
                ]
        )
      <*> frequency
        ( fmap pure
            <$> [ (4, 1.0)
                , (2, -1.0)
                , (4, 42.0)
                , (4, 283.93)
                ]
        )

instance GenExprWithContext Int where
  genExprWithContext ctx =
    frequency
      [ (1, Lit tshow <$> arbitrary)
      , (fromIntegral $ size ctx, genExpr ctx{size = size ctx `div` 2})
      ]

instance GenExprWithContext [Int] where
  genExprWithContext ctx = do
    n <- chooseInt (0, fromIntegral $ size ctx)
    Lit tshow <$> vectorOf n (chooseInt (0, fromIntegral (size ctx)))

instance (Typeable a, GenExprWithContext a, Typeable b, GenExprWithContext b) => GenExprWithContext (a, b) where
  genExprWithContext ctx = Tup <$> genExprWithContext ctx <*> genExprWithContext ctx

instance (Typeable a, GenExprWithContext a, Typeable b, GenExprWithContext b) => GenExprWithContext (Unboxed (a, b)) where
  genExprWithContext ctx = UTup <$> genExprWithContext ctx <*> genExprWithContext ctx

instance (Typeable b, Typeable a, GenExprWithContext a) => GenExprWithContext (b -> a) where
  genExprWithContext ctx =
    case Reflection.splitApps (typeRep @b) of
      (tupleTyCon, [Reflection.SomeTypeRep (x :: TypeRep (x :: kx)), Reflection.SomeTypeRep (y :: TypeRep (y :: ky))])
        | Just HRefl <- Reflection.typeRepKind x `eqTypeRep` typeRep @Type
        , Just HRefl <- Reflection.typeRepKind y `eqTypeRep` typeRep @Type
        , TypeRep <- x
        , TypeRep <- y
        , Just HRefl <- typeRep @(x, y) `eqTypeRep` typeRep @b -> do
            idX <- genFreshishId x ctx
            idY <- genFreshishId y ctx
            let ctx' =
                  ctx
                    { inScopeIds =
                        (newLocalVarFreq ctx * fromIntegral (length (inScopeIds ctx) + 1) * size ctx + 1, SomeScopedId (LocalId idX))
                          : (newLocalVarFreq ctx * fromIntegral (length (inScopeIds ctx)) * size ctx + 1, SomeScopedId (LocalId idY))
                          : inScopeIds ctx
                    }
            e <- genExpr ctx'
            pure $ Lam (Tuple (idX, idY)) e
      _ -> do
        freshishId <- genFreshishId typeRep ctx
        let ctx' = ctx{inScopeIds = (newLocalVarFreq ctx * fromIntegral (length (inScopeIds ctx)) * size ctx + 1, SomeScopedId (LocalId freshishId)) : inScopeIds ctx}
        e <- genExpr ctx'
        pure $ Lam (Wild freshishId) e
