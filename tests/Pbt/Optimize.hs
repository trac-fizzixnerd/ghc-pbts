{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE ExplicitNamespaces #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Pbt.Optimize where

import Control.Monad (replicateM)
import Control.Monad.IO.Class (MonadIO (liftIO))
import Data.Coerce (coerce)
import Data.Foldable (Foldable (foldl'))
import Data.Functor.Identity (Identity (..))
import Data.Generics.Product.Any (HasAny (the))
import Data.Generics.Sum.Any (AsAny (_As))
import Data.Kind (Type)
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import Data.Text.IO qualified as Text
import Data.Traversable (for)
import Debug.Trace (traceShowM)
import GHC.Float (clamp)
import GHC.Generics (Generic (..), Generically (..), K1 (..), M1 (..), U1, V1, type (:*:) (..), type (:+:) (..))
import GHC.Stack (HasCallStack)
import Optics (AffineTraversal', castOptic, coerced, coerced1, iso, over, under, (%), (&), (^.))
import Optics qualified
import Optics.AffineFold (preview)
import Optics.Optic ((<&>))
import Optics.Prism (Prism, Prism')
import Optics.Setter (set)
import Pbt.Driver (genDriver)
import Pbt.Expr (Context, GenExprWithContext, SomeScopedId, SomeTypeProxy, genExpr, interpretToHsSrc)
import Pbt.Expr.Utility (betaReduce, utility1)
import Pbt.Properties (averageBy, functions1, functionsUnboxedTuples, genTypeProxy1, genTypeProxyUnboxedTuples, guardBy, judge, simpleContext2)
import Test.QuickCheck (Gen, sample)
import Test.QuickCheck.Gen (generate, sample')
import Type.Reflection (TypeRep, pattern TypeRep)

class Select s where
  select :: s -> [AffineTraversal' s Double]
  default select :: (Generic s, GSelect (Rep s)) => s -> [AffineTraversal' s Double]
  select s =
    s
      & from @s
      & gselect
      <&> \t -> iso from to % t

class GSelect f where
  gselect :: f p -> [AffineTraversal' (f p) Double]

instance GSelect V1 where
  gselect _ = []

instance GSelect U1 where
  gselect _ = []

instance (GSelect f, GSelect g) => GSelect (f :+: g) where
  gselect (L1 x) = (_As @"L1" %) <$> gselect x
  gselect (R1 x) = (_As @"R1" %) <$> gselect x

instance (GSelect f, GSelect g) => GSelect (f :*: g) where
  gselect ((x :: f p) :*: (y :: g p)) = ((the @1 %) <$> gselect x) <> ((the @2 %) <$> gselect y)

instance (Select c) => GSelect (K1 i c) where
  gselect (K1 x) = (the @c %) <$> select x

instance (GSelect f) => GSelect (M1 i t f) where
  gselect (M1 x) = (the @1 %) <$> gselect x

newtype Optimizable = Optimizable {unOptimizable :: Double}
  deriving newtype (Eq, Ord, Show, Num, Fractional, Floating, Real, RealFrac)

instance Select Optimizable where
  select _ = [castOptic coerced]

instance Select Double where
  select _ = []

instance Select Int where
  select _ = []

instance Select Char where
  select _ = []

instance Select Text where
  select _ = []

data Bookus = Bookus Optimizable Double
  deriving stock (Generic)

instance (Select a, Select b) => Select (a, b)
instance (Select a) => Select [a]
instance Select SomeScopedId where select _ = []
instance Select SomeTypeProxy where select _ = []
instance Select (Context Optimizable)

grad :: (Fractional a) => a -> [AffineTraversal' s a] -> (s -> a) -> s -> [Maybe a]
grad h dxs f x0 = dxs <&> \dxn -> preview dxn x0 <&> \xn -> (f (set dxn (xn + h) x0) - f x0) / h

gradM :: (Monad m, Fractional a, Ord a, Show a) => (a -> a) -> [AffineTraversal' s a] -> (s -> m a) -> s -> m [Maybe a]
gradM h dxs f x0 = for dxs $ \dxn -> for (preview dxn x0) $ \xn -> do
  up <- f (set dxn (xn + h xn) x0)
  down <- f x0
  traceShowM $ "f x0 = " <> show down
  let quotient = (up - down) / h xn
  pure quotient

ascend :: (Fractional a, Ord a) => a -> (a -> a) -> [AffineTraversal' s a] -> [Maybe a] -> s -> s
ascend gamma h dxs dfdxs x0 =
  zip dxs dfdxs
    <&> (\(dx, dfdx) y -> y & over dx (\xn -> let xn' = xn + gamma * fromMaybe 0 dfdx in max xn' 1))
    & foldl' (\acc f -> f acc) x0

optimizeM :: (Show s, Ord a, Fractional a, Monad m, Show a) => a -> ([Maybe a] -> m Bool) -> (a -> a) -> [AffineTraversal' s a] -> (s -> m a) -> s -> m s
optimizeM gamma shouldStop h dxs f x0 = do
  traceShowM x0
  dfdxs <- gradM h dxs f x0
  let x0' = ascend gamma h dxs dfdxs x0
  isShouldStop <- shouldStop dfdxs
  if isShouldStop
    then pure x0'
    else optimizeM gamma shouldStop h dxs f x0'

optimize :: (Show s, Fractional a, Ord a, Show a) => a -> ([Maybe a] -> Bool) -> (a -> a) -> [AffineTraversal' s a] -> (s -> a) -> s -> s
optimize gamma shouldStop h dxs f x0 = runIdentity $ optimizeM gamma (Identity . shouldStop) h dxs (Identity . f) x0

shouldStopReporter :: (Show a, Ord a, Floating a) => a -> [Maybe a] -> IO Bool
shouldStopReporter tol dfdxs = do
  let gradLength = sqrt $ sum $ (^ 2) . fromMaybe 0 <$> dfdxs
  if gradLength > tol
    then do
      putStrLn "Finished."
      pure True
    else do
      putStrLn $ "gradLength: " <> show gradLength <> " > " <> show tol
      pure False

tol = 50
h x = 0.4 * x
gamma = 5

optimizeContext :: (GenExprWithContext a) => TypeRep a -> Context Optimizable -> IO (Context Optimizable)
optimizeContext (TypeRep :: TypeRep a) ctx = optimizeM gamma (shouldStopReporter tol) h (select ctx) (fmap (averageBy utility1) . replicateM 1000 . generate . genExpr @a . fmap round) ctx

test :: (HasCallStack) => IO (Context Int)
test = do
  ctx <- optimizeContext (TypeRep @(Int, Int)) (fromIntegral <$> simpleContext2 functions1 genTypeProxy1)
  Text.putStrLn . interpretToHsSrc . head =<< sample' (genExpr @(Int, Int) (round <$> ctx))
  pure $ round <$> ctx

{- averageOverNRunsM :: (MonadIO m, Fractional a) => Int -> s -> (s -> m a) -> m a
averageOverNRunsM n gen f =
  fmap sum . traverse f =<< liftIO (take n <$> sample' gen)
 -}