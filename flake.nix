{
  # inspired by: https://serokell.io/blog/practical-nix-flakes#packaging-existing-applications
  description = "A package for using property-based tests (PBTs) on GHC.";
  inputs.nixpkgs.url = "nixpkgs";
  outputs = { self, nixpkgs }:
    let
      supportedSystems = [ "x86_64-linux" "x86_64-darwin" ];
      forAllSystems = f: nixpkgs.lib.genAttrs supportedSystems (system: f system);
      nixpkgsFor = forAllSystems (system: import nixpkgs {
        inherit system;
        overlays = [ self.overlay ];
      });
    in
    {
      overlay = (final: prev: {
        ghc-pbts = final.haskell.packages.ghc94.callCabal2nix "ghc-pbts" ./. { };
      });
      packages = forAllSystems (system: {
        ghc-pbts = nixpkgsFor.${system}.ghc-pbts;
      });
      defaultPackage = forAllSystems (system: self.packages.${system}.ghc-pbts);
      checks = self.packages;
      devShell = forAllSystems (system:
        let haskellPackages = nixpkgsFor.${system}.haskell.packages.ghc94;
        in
        haskellPackages.shellFor {
          packages = p: [ self.packages.${system}.ghc-pbts ];
          withHoogle = true;
          buildInputs = with haskellPackages; [
             haskell-language-server
             ghcid
             cabal-install
             haskell-debug-adapter
          ];
        });
    };
}
